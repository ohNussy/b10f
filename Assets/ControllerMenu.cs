﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ControllerMenu : MonoBehaviour {

	
	public void OpenMenuScene(string scene)
	{
		SceneManager.LoadScene(scene);
	}

}
