﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class ControllerMenuChara : MonoBehaviour {

	[Header("Params")]
	public GameObject ParamPanel;
	public CharaParamList ParamList;
	public Button[] BtnTabList;

	BattlerData TempBattler;

	[Header("Items")]
	public SelectableItemList ItemList;

	[Header("Passive")]
	public GameObject PassivePanel;
	public GameObject PassiveFlexList;
	public Text LblMemory;

	[Header("Prefabs")]
	public GameObject PassiveRowPrefab;

	void OnEnable()
	{
		SetEquipList();
	}

	public void OpenPassivePanel()
	{
		SetPassiveList();
		SetMemory();
	}

	public void SetPassiveList()
	{
		PassivePanel.SetActive(true);
//		GOHelper.RemoveAllChildren(PassiveFlexList);
		int idx = 0;
		foreach (MasterPassive mp in Masters.GetAll<MasterPassive>())
		{
			GameObject go = PassiveFlexList.transform.GetChild(idx).gameObject;
			go.GetComponent<PassiveRow>().SetData(mp);
			idx++;
		}
    }

	public void SetMemory()
	{
		LblMemory.text = string.Format("{0}/{1}"
			, CharaData.Current.VaryingCurrency(CharaData.CURRENCY_IDX_MEMORY)
			, CharaData.Current.GainedCurrency(CharaData.CURRENCY_IDX_MEMORY)
		);
	}

	public void ClosePassive()
	{
		PassivePanel.SetActive(false);
	}

	public void OpenParamList()
	{
		// battler 生成
		TempBattler = GetComponent<BattlerData>();
		if (TempBattler != null)
		{
			Destroy(TempBattler);
			TempBattler = null;
		}
		TempBattler = BattlerData.GetFromCharaData(gameObject, CharaData.Current);
		// カテゴリ呼び出し
		ParamPanel.SetActive(true);
		SetParamListCategory(0);
    }

	public void SetParamListCategory(int category)
	{
        List<MasterItemParam> mips = (from m in Masters.GetAll<MasterItemParam>() where m.Category == category select m).ToList();
		List<CharaParamRow.CharaParamRowData> list = new List<CharaParamRow.CharaParamRowData>();
		foreach (MasterItemParam mip in mips)
		{
			if (!mip.ViewDefault && TempBattler.GetParamValue(mip.ParamKey) == 0)
				continue;
			list.Add(new CharaParamRow.CharaParamRowData() { mip = mip, Value = TempBattler.GetRealParam(mip.ParamKey) });
		}
		ParamList.SetData<CharaParamRow.CharaParamRowData>(list);
		for (int idx = 0; idx < BtnTabList.Count(); idx++)
			BtnTabList[idx].interactable = (idx != category);
    }

	public void CloseParamList()
	{
		ParamPanel.SetActive(false);
	}

	public void SetEquipList()
	{
		List<SelectableItemRow.SelectableItemRowData> list = new List<SelectableItemRow.SelectableItemRowData>();
		foreach (StoredItemData item in CharaData.Current.EquipList())
			list.Add(new SelectableItemRow.SelectableItemRowData() { item = item });
		ItemList.SetData<SelectableItemRow.SelectableItemRowData>(list);
    }

}
