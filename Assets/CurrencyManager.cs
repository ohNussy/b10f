﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class CurrencyManager : Singleton<CurrencyManager>
{

	public static CurrencyManager Instance
	{
		get
		{
			return ((CurrencyManager)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	[Header("Targets")]
	public GameObject TargetPanel;
	public Text[] LblCurrency;

	public static void OpenCurrency()
	{
		Instance.TargetPanel.SetActive(true);
		ResetCurrencyValue();
	}

	public static void CloseCurrency()
	{
		Instance.TargetPanel.SetActive(false);
	}

	public static void ResetCurrencyValue()
	{
		CharaData.Current.ResetVariedCurrency(); // reset vary currency
		if (Instance.LblCurrency.Count() > 0)
		{
			for (int idx = 0;idx < Instance.LblCurrency.Count(); idx++)
			{
				Instance.LblCurrency[idx].text = CharaData.Current.VaryingCurrency(idx);
			}
		}
	}

	public static void SetCurrencyPayingValue()
	{
		if (Instance.LblCurrency.Count() > 0)
		{
			for (int idx = 0; idx < Instance.LblCurrency.Count(); idx++)
			{
				Instance.LblCurrency[idx].text = CharaData.Current.VaryingCurrency(idx);
			}
		}
	}

}
