﻿using UnityEngine;
using UnityEditor;
using AvoEx;

public class ExchangeAssetCrypt
{

	[MenuItem("Assets/Encrypt Data")]
	static void ExportResource()
	{
		foreach (TextAsset obj in Selection.objects)
		{
			string result = AesEncryptor.Encrypt(obj.text);
			System.IO.File.WriteAllText(Application.dataPath + "/ForBundles/Data/" + obj.name + ".txt", result);
		}
	}

	[MenuItem("Assets/Dncrypt Data")]
	static void ResetCacheAssetBundle()
	{
		foreach (TextAsset obj in Selection.objects)
		{
			string result = AesEncryptor.DecryptString(obj.text);
			System.IO.File.WriteAllText(Application.dataPath + "/ForBundles/Data/" + obj.name + ".txt", result);
		}
	}


}
