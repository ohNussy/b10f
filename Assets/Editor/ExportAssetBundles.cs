﻿using UnityEngine;
using UnityEditor;

public class ExportAssetBundles
{

	[MenuItem("Assets/Build AssetBundle")]
	static void ExportResource()
	{
		Caching.CleanCache();
#if UNITY_ANDROID
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles/Android", new BuildAssetBundleOptions(), BuildTarget.Android);
#elif UNITY_IOS
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles/iOS", new BuildAssetBundleOptions(), BuildTarget.iOS);
#else
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles");
#endif
	}

	[MenuItem("Assets/Build AssetBundle (Android)")]
	static void ExportResourceAndroid()
	{
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles/Android", new BuildAssetBundleOptions(), BuildTarget.Android);
	}

	[MenuItem("Assets/Build AssetBundle (iOS)")]
	static void ExportResourceIos()
	{
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles/iOS", new BuildAssetBundleOptions(), BuildTarget.iOS);
	}

	[MenuItem("Assets/Build AssetBundle (Web, PC)")]
	static void ExportResourceWebpc()
	{
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles");
	}

	[MenuItem("Assets/Build AssetBundle (WebGL)")]
	static void ExportResourceWebGL()
	{
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles/WebGL", new BuildAssetBundleOptions(), BuildTarget.WebGL);
	}

	[MenuItem("Assets/Reset Cache AssetBundle")]
	static void ResetCacheAssetBundle()
	{
		Caching.CleanCache();
	}


}
