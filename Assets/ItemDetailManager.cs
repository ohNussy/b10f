﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemDetailManager : MonoBehaviour {

	[Header("Targets")]
	public CharaParamList ParamList;
	public SelectableItemRow ItemIcon;
	public Text LblName;
	public GameObject GOHand;
	public RawImage IconEquipType;
	public RawImage IconHandType;
	public Text LblEquipType;
	public Text LblHandType;
	public GameObject GOAction1;
	public GameObject GOAction2;
	public RawImage IconAction1;
	public RawImage IconAction2;
	public Text LblAction1;
	public Text LblAction2;

	public StoredItemData CurrentItem;

	public void SetItemDetail(StoredItemData item)
	{
		CurrentItem = item;
		// visual

    }

	public static void SetItemDetailAll(StoredItemData item)
	{
		foreach (ItemDetailManager idm in FindObjectsOfType<ItemDetailManager>())
			idm.SetItemDetail(item);
	}

}
