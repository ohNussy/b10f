﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class PassiveRow : GridRowBase {

	public MasterPassive ThisMaster;

	public Text LblCost;
	public RawImage ImgTarget;
	public GameObject GainedFill;
	public GameObject RestrictFill;

	public void SetData(MasterPassive mp)
	{
		ThisMaster = mp;
		LblCost.text = string.Format("<color=#{0}>{1}</color>", mp.CostColor, mp.MemoryCost.ToString());
		TextureManager.SetImage(ImgTarget, mp.ThisBase.DispImage, Color.white);
		Vector3 pos = new Vector3(70 * (float)mp.PosX, -70 * (float)mp.PosY, 0);
		GetComponent<RectTransform>().anchoredPosition = pos;
		GainedFill.SetActive(false);
		RestrictFill.SetActive(true);
		if (CharaData.Current.CanLearnPassive(mp))
			RestrictFill.SetActive(false);
		if (CharaData.Current.HasPassive(mp))
		{
			GainedFill.SetActive(true);
			RestrictFill.SetActive(false);
		}
	}

	public override void OnMouseOnEvent()
	{
		DialogManager.OpenToast(ThisMaster.ThisBase.DispDesc, ThisMaster.ThisBase.DispName);
	}

	public override void OnMouseOutEvent()
	{
		DialogManager.CloseToast();
	}

	public override void OnClickEvent()
	{
		CharaData.Current.SetVariedCurrency(CharaData.CURRENCY_IDX_MEMORY, -ThisMaster.MemoryCost);
		CurrencyManager.SetCurrencyPayingValue();
		// memory visual
		FindObjectOfType<ControllerMenuChara>().SetMemory();
		DialogManager.OpenConfirmDialog(string.Format(LangManager.GetString("TEXT_CONFIRM_GAIN_PASSIVE"), ThisMaster.MemoryCost.ToString(), ThisMaster.ThisBase.DispName)
			, LangManager.GetString("WORD_TITLE_CONFIRM"), "", "", (t) => {
				bool ForceFinish = false;
				if (!t)
					ForceFinish = true;
				// TODO : confirm
				if (CharaData.Current.HasPassive(ThisMaster))
					ForceFinish = true;
				if (!CharaData.Current.CanLearnPassive(ThisMaster))
					ForceFinish = true;
				if (!CharaData.Current.EnoughCurrency(CharaData.CURRENCY_IDX_MEMORY, ThisMaster.MemoryCost))
					ForceFinish = true;
				// force finish
				if (ForceFinish)
				{
					CharaData.Current.ResetVariedCurrency();
					// memory visual
					FindObjectOfType<ControllerMenuChara>().SetMemory();
					return;
				}
				// learn
				CharaData.Current.GainPassive(ThisMaster);
				// reset visual
				SetData(ThisMaster);
				FindObjectsOfType<PassiveRow>().ToList().ForEach((pr) => {
					if (pr.ThisMaster.IsNear(ThisMaster))
						pr.SetData(pr.ThisMaster);
				});

				// pay
				CharaData.Current.ApplyVariedCurrency();
				AudioManager.PlaySE("gold");
				// memory visual
				FindObjectOfType<ControllerMenuChara>().SetMemory();
			});


	}


}
