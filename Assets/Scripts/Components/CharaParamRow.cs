﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharaParamRow : GridRowBase
{

	[Header("Targets")]
	public Text LblName;
	public Text LblValue;

	public MasterItemParam ThisMaster;
	public double Value;

	public override void SetData(RowDataBase Row)
	{
		base.SetData(Row);
		ThisMaster = GetData<CharaParamRowData>().mip;
		Value = GetData<CharaParamRowData>().Value;
		LblName.text = ThisMaster.ThisBase.DispName;
		LblValue.text = Value.ToString("N0");
	}

	public void ClickHelp()
	{
		DialogManager.OpenToast(ThisMaster.ThisBase.DispDesc, ThisMaster.ThisBase.DispName);
	}

	public class CharaParamRowData : RowDataBase
	{
		public MasterItemParam mip;
		public double Value;
	}

}
