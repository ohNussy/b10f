﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AsyncManager : Singleton<AsyncManager> {

	public static AsyncManager Instance
	{
		get
		{
			return ((AsyncManager)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	List<System.Action> ActionList = new List<System.Action>();
	bool CanAct = false;

	void Awake()
	{
		Instance = this;
		DontDestroyOnLoad(gameObject);
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (CanAct)
		{
			System.Action Target = ActionList.FirstOrDefault();
			ActionList = ActionList.Skip(1).ToList();
			if (!object.ReferenceEquals(Target, null))
				Target();
			Target = null;
			CanAct = (ActionList.Count > 0);
		}
	}

	/*
	*	アクション
	*/

	public static void AddAction(System.Action NewAct)
	{
		if (NewAct != null)
		{
			Instance.ActionList.Add(NewAct);
			Instance.CanAct = true;
		}
	}
	
	/*
	*	ディレイアクション
	*/
	public static void AddDelayedAction(float WaitSeconds, System.Action NewAct)
	{
		Instance.StartCoroutine(Instance.DelayedAction(NewAct, WaitSeconds));
	}

	public IEnumerator DelayedAction(System.Action NewAct, float WaitSeconds)
	{
		yield return new WaitForSeconds(WaitSeconds);
		AddAction(NewAct);
	}

	/*
	*	アクションの簡易受け渡し
	*/
	public static void RunBoolReturn(System.Action<bool> Act, bool result)
	{
		if (!object.ReferenceEquals(Act, null))
			Act(result);
    }

}
