﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {

	public float DestroyTime = 0;
	
	// Update is called once per frame
	void Update ()
	{
		DestroyTime -= Time.deltaTime;
		if (DestroyTime <= 0)
			Destroy(gameObject);
    }
}
