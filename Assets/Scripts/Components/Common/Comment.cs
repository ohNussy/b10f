﻿using UnityEngine;
using System.Collections;

public class Comment : MonoBehaviour {

	[Multiline]
	public string Body; 

	// Use this for initialization
	void OnEnable () {
		Destroy(this);
	}
}
