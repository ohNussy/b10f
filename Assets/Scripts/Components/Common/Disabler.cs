﻿using UnityEngine;
using System.Collections;

public class Disabler : MonoBehaviour {

	public GameObject Target;
	public bool Flg = false;

	// Use this for initialization
	public void DoAction()
	{
		Target.SetActive(Flg);
	}

}
