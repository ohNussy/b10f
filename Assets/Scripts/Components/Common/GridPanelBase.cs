﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GridPanelBase : MonoBehaviour {

	[Header("GO")]

	public GameObject GridList;
	public GameObject PrefabStocks;

	[Header("Prefabs")]

	public GameObject RowPrefab;

	[Header("Data")]

	public bool UsePrefabStocks = false;

	public List<RowDataBase> DataList = new List<RowDataBase>();
	int RunningToken = 0;

	public bool IsDestroy = false;

	public void SetData <T>(List<T> RowList) where T : RowDataBase
	{
//		LoadingPanel.Open();
		if (UsePrefabStocks)
		{
			// set parents
			GridList.GetComponentsInChildren<GridRowBase>().ToList().ForEach((r) => {
				GOHelper.SetParent(r.gameObject, PrefabStocks);
				r.SetInvisiblePosition();
			});
        }
		else
		{
			// remove children
			GOHelper.RemoveAllChildren(GridList);
		}
		GameData.Instance.StartCoroutine(_SetData(RowList));
	}

	IEnumerator _SetData<T>(List<T> RowList) where T : RowDataBase
	{
		int ThisRunningToken = RunningToken = Random.Range(0, 100000000);
		foreach (RowDataBase row in RowList)
		{
			if (ThisRunningToken != RunningToken)
				break;
			if (!IsDestroy)
				AddNewRow<T>(row as T);
            yield return null;
		}
		if (!IsDestroy)
			AfterSetData();
		LoadingPanel.Close();
	}

	public virtual void AfterSetData()
	{
		// override for Set Type Rows
	}

	public virtual void SetDataForType(GridRowBase row)
	{
		// override for Set Type Rows
	}

	public GameObject AddNewRow<T>(T row, GameObject prefab = null) where T : RowDataBase
	{
		if (GridList == null)
			return null;
		if (!GridList.activeSelf)
			return null;
		if (prefab == null)
			prefab = RowPrefab;
		GameObject go;
        if (UsePrefabStocks)
		{
			if (PrefabStocks.GetComponentsInChildren<GridRowBase>().Length > 0)
			{
				go = PrefabStocks.GetComponentInChildren<GridRowBase>().gameObject;
			}
			else
			{
				// is new 
				go = GOHelper.AddLoadedUI(prefab, GridList.transform);
			}
			GOHelper.SetParent(go, GridList);
		}
		else
		{
			go = GOHelper.AddLoadedUI(prefab, GridList.transform);
		}
		GridRowBase grb = go.GetComponent<GridRowBase>();
		grb.SetData(row);
		SetDataForType(grb);
		return go;
	}

	public GameObject AddNewBlankRow(GameObject prefab = null)
	{
		if (prefab == null)
			prefab = RowPrefab;
		GameObject go;
		if (UsePrefabStocks)
		{
			if (PrefabStocks.GetComponentsInChildren<GridRowBase>().Length > 0)
			{
				go = PrefabStocks.GetComponentInChildren<GridRowBase>().gameObject;
			}
			else
			{
				// is new 
				go = GOHelper.AddLoadedUI(prefab, GridList.transform);
			}
			GOHelper.SetParent(go, GridList);
		}
		else
		{
			go = GOHelper.AddLoadedUI(prefab, GridList.transform);
		}
		GridRowBase grb = go.GetComponent<GridRowBase>();
		grb.SetBlank();
		return go;
	}

	public void SetSample()
	{
		SetData(new List<RowDataBase>() { new RowDataBase() , new RowDataBase() , new RowDataBase() });
	}

	void OnDestroy()
	{
		IsDestroy = true;
	}
}
