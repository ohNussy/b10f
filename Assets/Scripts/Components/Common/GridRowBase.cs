﻿using UnityEngine;
using System.Collections;

public class GridRowBase : TouchableBehavior
{

	public RowDataBase ThisData;

	public T GetData<T>() where T : RowDataBase	{
        return ThisData as T;
	}

	public void SetInvisiblePosition()
	{
		GetComponent<RectTransform>().anchoredPosition = new Vector3(2000, 2000, 0);
	}

	public virtual void SetData(RowDataBase Row)
	{
		// override each RowType
		ThisData = Row;
	}

	public virtual void SetBlank()
	{
		// override each RowType
	}
}
