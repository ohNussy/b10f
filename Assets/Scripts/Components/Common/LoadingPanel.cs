﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingPanel : Singleton<LoadingPanel> {

	[Header("GO")]
	public GameObject panel;
	public Text lbl;

	public static LoadingPanel Instance
	{
		get
		{
			return ((LoadingPanel)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	public static void Open(string body = "")
	{
		if (Instance != null && Instance.panel != null)
		{
			Instance.lbl.text = body;
			Instance.panel.SetActive(true);
		}
	}

	public static void Close()
	{
		if (Instance != null && Instance.panel != null)
			Instance.panel.SetActive(false);
	}


}
