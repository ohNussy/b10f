﻿using UnityEngine;
#if UNITY_IOS
using UnityEngine.iOS;
#endif
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class NotificationManager : Singleton<NotificationManager> {

	public static NotificationManager Instance
	{
		get
		{
			return ((NotificationManager)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	List<int> IdList = new List<int>();
	static int id = 0;

	public static void AddNotification(string title, string body, long delay)
	{
#if UNITY_IOS
		UnityEngine.iOS.LocalNotification l = new UnityEngine.iOS.LocalNotification();
		l.applicationIconBadgeNumber = 1 + id;
		l.fireDate = System.DateTime.Now.AddSeconds(delay);
		l.alertBody = body;
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(l);
		id++;
#elif UNITY_ANDROID
		Instance.IdList.Add(id);
        LocalNotification.SendNotification(id, delay, title, body, Color.grey);
		id++;
#endif
	}

	public static void ResetNotification()
	{
#if UNITY_IOS
		UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
#elif UNITY_ANDROID
		Instance.StartCoroutine(Instance._CancelNotification());
#endif
		id = 0;
	}

	public IEnumerator _CancelNotification()
	{
		for (int i = 0; i < 10; i++)
		{
			LocalNotification.CancelNotification(i);
			yield return null;
		}
		Instance.IdList.Clear();
	}


}
