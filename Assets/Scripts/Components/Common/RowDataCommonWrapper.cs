﻿using UnityEngine;
using System.Collections;

public class RowDataCommonWrapper : RowDataBase
{

	public object data;

	public void SetData<Type>(object obj) where Type : Object
	{
		data = obj as Type;
	}

	public Type GetData<Type>() where Type : Object
	{
		return data as Type;
	}

}
