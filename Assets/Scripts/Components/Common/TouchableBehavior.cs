﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class TouchableBehavior : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler
{

	public bool MouseDown = false;
	public float DownTime = 0f;
	public bool IsClickedOnce = false;

	void Update()
	{
		if (MouseDown)
		{
			DownTime += Time.deltaTime;
			if (DownTime > 0.3f)
			{
				EndMouseDown();
				OnLongClickEvent();
			}
		}
	}

	public void OnPointerClick(PointerEventData e)
	{
		if (IsClickedOnce)
			OnClick2ndEvent();
		else
		{
			IsClickedOnce = true;
			OnClickEvent();
		}
	}

	public void OnPointerDown(PointerEventData e)
	{
		MouseDown = true;
	}

	public void OnPointerUp(PointerEventData e)
	{
		EndMouseDown();
	}

	public void OnPointerEnter(PointerEventData e)
	{
		if (Input.GetMouseButton(0))
		{
			if (IsClickedOnce)
				OnDragIn2ndEvent();
			else if (!GameData.TouchDevice)
				OnDragInEvent();
		}
		else
		{
			OnMouseOnEvent();
		}
	}

	public void OnPointerExit(PointerEventData e)
	{
		EndMouseDown();
		OnMouseOutEvent();
    }

	void EndMouseDown()
	{
		MouseDown = false;
		DownTime = 0;
	}

	public virtual void OnClickEvent()
	{
		// assign unique function
	}

	public virtual void OnClick2ndEvent()
	{
		// assign unique function
		OnClickEvent();
	}

	public virtual void OnLongClickEvent()
	{
		// assign unique function
	}

	public virtual void OnDragInEvent()
	{
		// assign unique function
	}

	public virtual void OnDragIn2ndEvent()
	{
		// assign unique function
		OnDragInEvent();
	}

	public virtual void OnMouseOnEvent()
	{
		// assign unique function
	}

	public virtual void OnMouseOutEvent()
	{
		// assign unique function
	}

	// 複数選択可能な項目で、選択対象を変更した場合に表示する
	public virtual void OnSelectedEvent(bool IsSelected)
	{
		// assign unique function
	}

}
