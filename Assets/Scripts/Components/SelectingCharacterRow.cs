﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectingCharacterRow : GridRowBase
{

	[Header("Targets")]
	public RawImage ImgTarget;
	public Text LblBody;

	public override void SetData(RowDataBase Row)
	{
		base.SetData(Row);
		CharaData chara = GetData<SelectingCharacterRowData>().ThisChara;
        LblBody.text = string.Format("{0}\nLv{1}\n{2}F{3}", chara.Data.DispName, chara.Data.CurrentLevel, chara.Data.ClearedFloor, chara.ClearedLoopText);
		TextureManager.SetImageByURL(ImgTarget, chara.ImageUrl, Color.white);
    }

	public class SelectingCharacterRowData : RowDataBase
	{
		public CharaData ThisChara;
	}
}
