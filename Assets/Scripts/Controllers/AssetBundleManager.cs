﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AssetBundleManager : Singleton<AssetBundleManager>
{

	public static AssetBundleManager Instance
	{
		get
		{
			return ((AssetBundleManager)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	// static
	public static string CRYPT_PASS = "J7dNV9Z42qRLPghXll0uuQnotf7iZ4OcVuD7YTA1l99GjreDV17zaA805EiYpGdnRvGYpWEYDp25Z80s0ExhzmBEygzLJK6awfrzrXIjkCrm4CNqMo8WChubtVZkhnZJ";

	Dictionary<string, AssetBundle> abs = null;

	public string[] TargetAssets;
	public bool BundlesPrepared = false;

	public void Init(System.Action<bool> OnEnd)
	{
		StartCoroutine(_Init(OnEnd));
	}

	IEnumerator _Init(System.Action<bool> OnEnd)
	{
		abs = new Dictionary<string, AssetBundle>();
		bool result = true;
		foreach (string key in TargetAssets)
		{
			string url = AssetBundleDataPath(key);
			if (GameData.Instance.AutoClearCache)
				Caching.CleanCache();
			WWW w = WWW.LoadFromCacheOrDownload(url, 0);
			yield return w;
			if (string.IsNullOrEmpty(w.error))
			{
				abs[key] = w.assetBundle;
			}
			else
			{
				GameData.Error("AssetBundle Init Error!@" + key + "\n" + url + "\n" + w.error);
				result = false;
				break;
			}

			yield return null;
		}
		if (result)
			BundlesPrepared = true;
		AsyncManager.RunBoolReturn(OnEnd, result);
	}

	string AssetBundleDataPath(string key)
	{
		// for debug
		if (GameData.IsDebugMode)
			return "file://" + Application.dataPath + "/AssetBundles/" + key;
		// for production
		return string.Format("http://s3-ap-northeast-1.amazonaws.com/b10f/v{0}/{1}/{2}", GameData.ThisVersion, GameData.PlatformName(), key);
	}

	public static void GetAsset<T>(string asset, string key, System.Action<bool, T> OnEnd) where T : Object
	{
		if (!Instance.abs.ContainsKey(asset))
		{
			GameData.Error("GetAsset Error! There is no Asset.:" + asset);
			OnEnd(false, null);
			return;
		}
		if (!Instance.abs[asset].Contains(key))
		{
			GameData.Error("GetAsset Error! There is no Data.:" + key + " @" + asset);
			OnEnd(false, null);
			return;
		}
		AssetBundleRequest req = Instance.abs[asset].LoadAssetAsync<T>(key);
		Instance.StartCoroutine(Instance._GetAsset<T>(req, OnEnd));
	}

	IEnumerator _GetAsset<T>(AssetBundleRequest req, System.Action<bool, T> OnEnd) where T : Object
	{
		yield return req;
		T result = req.asset as T;
		if (result == null)
		{
			GameData.Error("GetAsset Error! Failed to load asset.");
			OnEnd(false, null);
		}
		else
		{
			OnEnd(true, result);
		}
	}

}
