﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class AudioManager : Singleton<AudioManager>
{

	public static AudioManager Instance
	{
		get
		{
			return ((AudioManager)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	float Check = 0;

	void Update()
	{
		if (Check > 0)
		{
			Check -= Time.deltaTime;
			if (Check <= 0)
			{
				Check = 0;
				int count = Instance.GetComponents<AudioSource>().Length;
				if (count > 1)
				{
					// 末尾以外
					foreach (AudioSource source in Instance.GetComponents<AudioSource>().Take(count - 1))
						if (source.loop)
							Instance.StartCoroutine(FadeOutBGM(source));
				}
			}
		}
	}

	public static void PlayBGM(string key)
	{
		// 既に同曲をプレイ中なら無視する
		if (Instance.IsPlayingBGM(key))
			return;
		// asset 取得
		AssetBundleManager.GetAsset<AudioClip>("audio", key, (r, ac) => {
			if (!r)
			{
				GameData.Error("PlayBgm Error! @" + key);
				return;
			}

			try
			{
				AudioClip clip = ac;
				StopBGM();

				// 再生
				AudioSource source = Instance.gameObject.AddComponent<AudioSource>();
				source.clip = clip;
				source.volume = 0;
				source.loop = true;
				source.Play();
				Instance.StartCoroutine(FadeInBGM(source));
			}
			catch (System.Exception ex)
			{
				Debug.LogWarning(ex.ToString());
			}
		});
	}

	public bool IsPlayingBGM(string key)
	{
		foreach (AudioSource source in Instance.GetComponents<AudioSource>())
		{
			if (source.clip == null)
				continue;
			if (source.clip.name == key)
				return true;
		}
		return false;
	}

	public static void StopBGM()
	{
		foreach (AudioSource source in Instance.GetComponents<AudioSource>())
		{
			if (source.loop)
				Instance.StartCoroutine(FadeOutBGM(source));
		}
	}

	public static void SetBGMVolume(float value)
	{
		foreach (AudioSource source in Instance.GetComponents<AudioSource>())
			source.volume = value;
	}

	public static void PlaySE(string key)
	{
		AssetBundleManager.GetAsset<AudioClip>("audio", key, (r, ac) =>
		{
			if (!r)
			{
				GameData.Error("PlaySE Error! @" + key);
				return;
			}

			try
			{
				AudioClip clip = ac;

				// 再生
				AudioSource.PlayClipAtPoint(clip, Vector3.zero, 1); // TODO : userdata sevolume
			}
			catch (System.Exception ex)
			{
				GameData.Warning(ex.ToString());
			}
		});
	}

	public static IEnumerator FadeOutBGM(AudioSource source)
	{
		source.loop = false;
		float StartTime = Time.realtimeSinceStartup;
		float DefaultVolume = source.volume;
		while (Time.realtimeSinceStartup < StartTime + 1)
		{
			float PassTime = Time.realtimeSinceStartup - StartTime;
			source.volume = DefaultVolume * (1f - PassTime);

			yield return null;
		}
		Destroy(source);
	}

	public static IEnumerator FadeInBGM(AudioSource source)
	{
		float StartTime = Time.realtimeSinceStartup;
		float TargetVolume = 1; // TODO : userdata bgmvolume
		while (Time.realtimeSinceStartup < StartTime + 1)
		{
			float PassTime = Time.realtimeSinceStartup - StartTime;
			source.volume = TargetVolume * PassTime;
			yield return null;
		}
		source.volume = TargetVolume;
	}

}
