﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;

public class ControllerInit : MonoBehaviour
{

	void Awake()
	{
		QualitySettings.vSyncCount = 0;
		ES2.Init();
		UnityEngine.Analytics.Analytics.CustomEvent("StartApp", new Dictionary<string, object>() { { "platform", Application.platform.ToString() } });
    }

	void Start()
	{
		DialogManager.OpenFilledLoading("Now Loading...");
		LoadAssetBundle();
    }

	void LoadAssetBundle()
	{
		DialogManager.OpenFilledLoading("Now Loading... Loading Asset Data.");
		AssetBundleManager.Instance.Init((t) => {
			if (!t)
			{

				return;
			}
			LoadMasterFromAB();
		});
	}

	void LoadMasterFromAB()
	{
		DialogManager.OpenFilledLoading("Now Loading... Loading Master Data.");
		Masters.Instance.Init((t) =>
		{
			if (!t)
			{
				return;
			}
			LoadLangFromAB();
		});
	}

	void LoadLangFromAB()
	{
		LangManager.LoadLang((t) => {
			if (!t)
			{
				return;
			}
			LoadUserData();
		});
	}

	void LoadUserData()
	{
		DialogManager.OpenFilledLoading("Now Loading... Loading User Data.");
		if (UserData.DoLoadUserData())
		{
			// logined
			LoadCurrentChara();
		}
		else
		{
			// register
			LoadCurrentChara();
		}
	}

	void LoadCurrentChara()
	{
		DialogManager.OpenFilledLoading("Now Loading... Loading Chara Data.");
		UserData.Instance.SetRecentChara();
		FinishAllInit();
    }

	void FinishAllInit()
	{
		DialogManager.OpenFilledLoading("Now Loading... Finishing Initialize.");
		SceneManager.LoadScene("title");
	}

}
