﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ControllerTitle : MonoBehaviour {

	[Header("Targets")]
	public SelectingCharacterRow ThisCharaRow;

	// Use this for initialization
	void Start () {
		DialogManager.CloseFilledLoading();
		if (!object.ReferenceEquals(ThisCharaRow, null))
		{
			// chara view
			ThisCharaRow.SetData(new SelectingCharacterRow.SelectingCharacterRowData() { ThisChara = CharaData.Current });
        }
    }

	public void OpenSelectChara()
	{

	}

	public void GameStart()
	{
		CurrencyManager.OpenCurrency();
		SceneManager.LoadScene("menu");	
	}

	public void OpenSupportURL()
	{

	}

}
