﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class DialogManager : Singleton<DialogManager> {

	public static DialogManager Instance
	{
		get
		{
			return ((DialogManager)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	[Header("Select Item Dialog")]
	public GameObject TargetPanelSI;
	System.Action<bool, StoredItemData> OnEndSI;

	[Header("Item Detail Dialog")]
	public GameObject TargetPanelID;

	[Header("Toast")]
	public GameObject TargetPanelTS;
	public RectTransform RT_TS;
	public Text LblTSBody;
	Vector2 Vec2_TS;

	[Header("Dialog")]
	public GameObject TargetPanelDL;
	System.Action<bool> OnEndDL;

	[Header("Config Dialog")]
	public GameObject TargetPanelCF;
	public Text LblBodyCF;
	public Text LblOKCF;
	public Text LblCancelCF;
	System.Action<bool> OnEndCF;

	[Header("Confirm Dialog")]
	public GameObject TargetPanelCD;

	[Header("Filled Loading")]
	public GameObject TargetPanelFL;
	public Text LblBodyFL;

	[Header("Easy Loading")]
	public GameObject TargetPanelEL;

	[Header("Company Dialog")]
	public GameObject TargetPanelCM;

	/*
	*	Select Item
	*/
	public static void OpenSelectItem(List<StoredItemData> Items, System.Action<bool, StoredItemData> OnEnd = null)
	{
		Instance.OnEndSI = OnEnd;
		Instance.TargetPanelSI.SetActive(true);
	}

	public static void CloseSelectItem(StoredItemData selected)
	{
		Instance.TargetPanelSI.SetActive(false);
		Instance.OnEndSI((selected != null), selected);
	}

	public void CloseSelectItemNoSelect()
	{
		CloseSelectItem(null);
	}

	/*
	*	Item Detail
	*/
	public static void OpenItemDetail(StoredItemData item)
	{

	}

	/*
	*	Toast
	*/
	static int PlayingToast;
	static bool ToastClosing = false;
	public static void OpenToast(string body, string title = "")
	{
		ToastClosing = false;
		PlayingToast = Random.Range(0, 100000000);
        Instance.TargetPanelTS.SetActive(true);
		Instance.Vec2_TS = Instance.RT_TS.anchoredPosition;
		Instance.LblTSBody.text = body;
		if (!string.IsNullOrEmpty(title))
			Instance.LblTSBody.text = string.Format("<size=18><color=#FB0>{0}</color></size> ", title) + Instance.LblTSBody.text;
		// tween animate
		iTween.Stop(Instance.gameObject);
		Instance.SetToastPos(-275);
		iTween.ValueTo(Instance.gameObject, iTween.Hash("from", -275, "to", -175, "time", 1.2f, "easetype", iTween.EaseType.easeOutQuart, "onupdate", "SetToastPos"));
		int CurrentPlayingToast = PlayingToast;
        AsyncManager.AddDelayedAction(4f, () => {
			if (CurrentPlayingToast != PlayingToast)
				return;
			CloseToast();
        });
	}

	public static void CloseToast()
	{
		// tween animate
		if (ToastClosing)
			return;
		int CurrentPlayingToast = PlayingToast = Random.Range(0, 100000000);
		ToastClosing = true;
		iTween.Stop(Instance.gameObject);
		Instance.SetToastPos(-175);
		iTween.ValueTo(Instance.gameObject, iTween.Hash("from", -175, "to", -275, "time", 1.2f, "easetype", iTween.EaseType.easeInQuart, "onupdate", "SetToastPos"));
		AsyncManager.AddDelayedAction(4f, () => {
			ToastClosing = false;
			if (CurrentPlayingToast != PlayingToast)
				return;
			iTween.Stop(Instance.gameObject);
			Instance.TargetPanelTS.SetActive(false);
		});
	}

	void SetToastPos(float y)
	{
		Vec2_TS.y = y;
		RT_TS.anchoredPosition = Vec2_TS;
	}

	/*
	*	Dialog
	*/
	public static void OpenDialog(string body, string title = "", System.Action OnEnd = null)
	{

	}

	/*
	*	Confirm Dialog
	*/
	public static void OpenConfirmDialog(string body, string title = "", string ok = "", string cancel = "", System.Action<bool> OnEnd = null)
	{
		Instance.LblBodyCF.text = body;
		if (!string.IsNullOrEmpty(title))
			Instance.LblBodyCF.text = string.Format("<size=24><color=#FB0>{0}</color></size>", title) + "\n" + Instance.LblBodyCF.text;
        Instance.LblOKCF.text = ok;
		if (string.IsNullOrEmpty(ok))
			Instance.LblOKCF.text = LangManager.GetString("WORD_OK");
		Instance.LblCancelCF.text = cancel;
		if (string.IsNullOrEmpty(cancel))
			Instance.LblCancelCF.text = LangManager.GetString("WORD_CANCEL");
		Instance.OnEndCF = OnEnd;
		Instance.TargetPanelCF.SetActive(true);
	}

	public void CloseConfirmDialog(bool result)
	{
		Instance.TargetPanelCF.SetActive(false);
		AsyncManager.RunBoolReturn(OnEndCF, result);
	}

	/*
	*	Open Filled Loading
	*/
	public static void OpenFilledLoading(string body = "")
	{
		Instance.TargetPanelFL.SetActive(true);
		Instance.LblBodyFL.text = body;
	}

	public static void CloseFilledLoading()
	{
		Instance.TargetPanelFL.SetActive(false);
	}

	/*
	*	Open Easy Loading
	*/
	public static void OpenEasyLoading()
	{

	}

	/*
	*	Company
	*/
	public static void CloseCompanyDialog()
	{

	}




}
