﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using AvoEx;

public class LangManager : Singleton<LangManager> {

	public static LangManager Instance
	{
		get
		{
			return ((LangManager)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	//static

	public static string ASSET_NAME = "lang";
	public static string KEY_JP = "lang";

	// <key, lang, value>
	public bool FinishLoad = false;
	public Dictionary<string, Dictionary<string, string>> StoredData = new Dictionary<string, Dictionary<string, string>>();
	
	public static void LoadLang(System.Action<bool> OnEnd)
	{
		AssetBundleManager.GetAsset<TextAsset>("lang", "lang", (r, t) => {
			if (!r)
			{
				GameData.Error("LangManager LoadLang Failed!");
				OnEnd(false);
                return;
			}
			// load json
			Instance.StoredData.Clear();
            List<Dictionary<string, string>> list = JsonMapper.ToObject<List<Dictionary<string, string>>>(AesEncryptor.DecryptString(t.text));
			foreach (Dictionary<string, string> row in list)
			{
				string key = row["key"];
				Instance.StoredData[key] = row;
			}
			Instance.FinishLoad = (Instance.StoredData != null);
            OnEnd(Instance.FinishLoad);
		});
	}

	public static string GetString(string key)
	{
		if (!Instance.FinishLoad)
			return "";
		if (Instance.StoredData.ContainsKey(key))
		{
			try
			{
				return (GameData.IsJp ? Instance.StoredData[key]["jp"].Replace("__", "\n") : Instance.StoredData[key]["en"].Replace("__", "\n"));
			}
			catch (System.Exception ex)
			{
				Debug.LogError(ex.ToString());
			}
		}
		Debug.LogWarning("LangMaster cant find key. : " + key);
		return "";
	}

}
