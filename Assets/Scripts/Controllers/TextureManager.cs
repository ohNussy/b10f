﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextureManager : Singleton<TextureManager>
{
	public static TextureManager Instance
	{
		get
		{
			return ((TextureManager)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	public static void SetImage(RawImage Target, string key, Color FinishColor, System.Action<bool> OnEnd = null)
	{
		Target.color = new Color(0, 0, 0, 0);

		AssetBundleManager.GetAsset<Texture>("texture", key, (r, tex) => {
			if (!r)
			{
				GameData.Error("TextureManager SetImage Failed! @" + key);
				AsyncManager.RunBoolReturn(OnEnd, false);
				return;
			}
			if (object.ReferenceEquals(Target,null))
			{
				GameData.Warning("TextureManager SetImage Canceled! @" + key);
				AsyncManager.RunBoolReturn(OnEnd, false);
				return;
			}
			try
			{
				Target.texture = tex;
				if (Target.texture == null)
				{
					Target.color = new Color(0, 0, 0, 0);
				}
				else
				{
					Target.SetNativeSize();
					Target.color = FinishColor;
				}
				AsyncManager.RunBoolReturn(OnEnd, true);
			}
			catch (System.Exception ex)
			{
				GameData.Error(ex.ToString());
				AsyncManager.RunBoolReturn(OnEnd, false);
			}
		});
	}

	public static void SetImageByURL(RawImage Target, string url, Color FinishColor, System.Action<bool> OnEnd = null)
	{
		Target.color = new Color(0, 0, 0, 0);
		Instance.StartCoroutine(_SetImageByURL(Target, url, FinishColor, OnEnd));
	}

	static IEnumerator _SetImageByURL(RawImage Target, string url, Color FinishColor, System.Action<bool> OnEnd = null)
	{
		using (WWW w = new WWW(url))
		{
			yield return w;
			if (!string.IsNullOrEmpty(w.error))
			{
				GameData.Error("TextureManager _SetImageByURL Failed! @" + url + "\n" + w.error);
				AsyncManager.RunBoolReturn(OnEnd, false);
			}
			else
			{
				Target.texture = w.texture;
				if (Target.texture == null)
				{
					Target.color = new Color(0, 0, 0, 0);
				}
				else
				{
					Target.SetNativeSize();
					Target.color = FinishColor;
				}
				AsyncManager.RunBoolReturn(OnEnd, true);
			}
		}
	}

}
