﻿using UnityEngine;
using System.Collections;

public class DateHelper : ScriptableObject
{

	public static long Tick()
	{
		return Tick(System.DateTime.Now);
	}

	public static long Tick(System.DateTime time)
	{
		return (time.Ticks / System.TimeSpan.TicksPerSecond);
	}

	public static string TimeDiff(System.DateTime a, System.DateTime b)
	{
		double diff = (a - b).TotalSeconds;
		if (diff < 60)
		{
			return (GameData.IsJp ? "1分以内" : "1min-");
		}
		else if (diff < 3600)
		{
			int min = Mathf.FloorToInt((float)(diff / 60f));
			return string.Format((GameData.IsJp ? "{0}分" : "{0}min"), min);
		}
		else if (diff < 86400)
		{
			int hour = Mathf.FloorToInt((float)(diff / 3600f));
			return string.Format((GameData.IsJp ? "{0}時間" : "{0}hour"), hour);
		}
		else
		{
			int day = Mathf.FloorToInt((float)(diff / 86400f));
			return string.Format((GameData.IsJp ? "{0}日" : "{0}day"), day);
		}
	}

	public static string GetDateString(System.DateTime dt)
	{
		return dt.Year.ToString("0000") + "/" + dt.Month.ToString("00") + "/" + dt.Day.ToString("00");
	}

	public static string GetTimeString(System.DateTime dt)
	{
		return dt.Hour.ToString("00") + ":" + dt.Minute.ToString("00") + ":" + dt.Second.ToString("00");
	}

	public static string GetTimeShortString(System.DateTime dt)
	{
		return dt.Hour.ToString("00") + ":" + dt.Minute.ToString("00");
	}

	public static string SecToTimeFormat(long sec)
	{
		System.TimeSpan ts = (GameData.RealTime.AddSeconds(sec) - GameData.RealTime);
		return string.Format("{0}:{1}:{2}"
			, ts.Hours.ToString("00")
			, ts.Minutes.ToString("00")
			, ts.Seconds.ToString("00")
			/*, (ts.Milliseconds < 1000 ? ts.Milliseconds / 10f : 0).ToString("00")*/);
	}
}
