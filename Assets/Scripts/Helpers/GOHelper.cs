﻿ using UnityEngine;
using System.Collections;

public class GOHelper : ScriptableObject
{

	public static void SetParent(GameObject target, GameObject parent)
	{
		target.transform.SetParent(parent.transform);
	}

	public static void RemoveAllChildren(GameObject target)
	{
		if (target.transform.childCount > 0)
		{
			for (int i = 0; i < target.transform.childCount; i++)
			{
				GameObject child = target.transform.GetChild(i).gameObject;
				GameObject.Destroy(child);
			}
		}
	}

	public static void InactiveAllChildren(GameObject target)
	{
		if (target.transform.childCount > 0)
		{
			for (int i = 0; i < target.transform.childCount; i++)
			{
				GameObject child = target.transform.GetChild(i).gameObject;
				child.SetActive(false);
			}
		}
	}

	public static void RemoveAllChildrenForEditor(GameObject target)
	{
		if (target.transform.childCount > 0)
		{
			while (target.transform.childCount > 0)
			{
				GameObject child = target.transform.GetChild(0).gameObject;
				GameObject.DestroyImmediate(child);
			}
		}
	}

	public static GameObject AddNewUI(GameObject prefab, Transform parent = null)
	{
		GameObject NewRow = GameObject.Instantiate(prefab) as GameObject;
		NewRow.transform.SetParent(parent);
		NewRow.transform.localScale = Vector3.one;
		return NewRow;
	}

	public static GameObject AddNewUI(Transform parent = null)
	{
		GameObject NewRow = new GameObject();
		NewRow.transform.SetParent(parent);
		NewRow.transform.localScale = Vector3.one;
		return NewRow;
	}

	public static IEnumerator AddNewUIAsync(GameObject prefab, Transform parent = null, System.Action<GameObject> OnEndAct = null)
	{
		prefab.SetActive(false);
		yield return null;
		GameObject NewRow = GameObject.Instantiate(prefab) as GameObject;
		NewRow.transform.SetParent(parent);
		NewRow.transform.localScale = Vector3.one;
		yield return null;
		NewRow.SetActive(true);
		prefab.SetActive(true);
		if (OnEndAct != null)
			OnEndAct(NewRow);
	}

	public static GameObject AddLoadedUI(GameObject prefab, Transform parent = null)
	{
		GameObject NewRow = AddNewUI(prefab, parent);
		RectTransform rt = NewRow.GetComponent<RectTransform>();
		rt.anchoredPosition = prefab.GetComponent<RectTransform>().anchoredPosition;
		rt.sizeDelta = prefab.GetComponent<RectTransform>().sizeDelta;
		return NewRow;
	}

}
