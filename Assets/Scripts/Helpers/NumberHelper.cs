﻿using UnityEngine;
using System.Collections;

public class NumberHelper : ScriptableObject
{

	// Use this for initialization
	public static long SafetyGainInt(long a, long b, long max)
	{
		a += b;
		// 0以下になる場合
		if (a < 0 && b < 0)
		{
			a = 0;
		}
		// 増加なのに0以下になる場合＝桁あふれ
		else if (a < 0 && b > 0)
		{
			a = max;
		}
		return a;
	}

	public static int Range(int min, int value, int max)
	{
		return Mathf.Max(min, Mathf.Min(value, max));
	}

	public static double Range(double min, double value, double max)
	{
		if (min > value)
			return min;
		if (value > max)
			return max;
		return value;
	}

	public static float Range(float min, float value, float max)
	{
		if (min > value)
			return min;
		if (value > max)
			return max;
		return value;
	}

	public static string PlusMinus(int value)
	{
		return (value > 0 ? "+" + value.ToString() : value.ToString());
	}

}
