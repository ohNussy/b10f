﻿using UnityEngine;
using System.Collections;
using LitJson;

public class SaveHelper : ScriptableObject {

	const string ENCRYPT_KEY = "D0pl33dbXqnPwHCcrXKxiC0N6Zx241Ag";

	// Use this for initialization
	public static void SaveData (string key, string data) 
	{
		ES2.Save(data, key + "?encrypt=true&password=" + ENCRYPT_KEY);
	}
	
	// Update is called once per frame
	public static string LoadData(string key)
	{
		string path = key + "?encrypt=true&password=" + ENCRYPT_KEY;
		if (ES2.Exists(path))
		{
			return ES2.Load<string>(path);
		}
		else
		{
			return null;
		}
	}

	public static bool ExistSave(string key)
	{
		
		return ES2.Exists(key);
	}

	public static void Delete(string key)
	{
		if (ExistSave(key))
			ES2.Delete(key);
	}

	public static void Reset()
	{
		ES2.DeleteDefaultFolder();
	}
	

}
