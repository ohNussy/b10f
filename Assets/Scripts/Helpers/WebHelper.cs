﻿using UnityEngine;
using System.Collections;

public class WebHelper : ScriptableObject {

	public static string EscapedURL(string url, string param)
	{
		return url + WWW.EscapeURL(param);
	}

	public static void OpenURL(string EscapedURL)
	{
#if UNITY_WEBPLAYER && !UNITY_EDITOR
		Application.ExternalCall("window.open('" + EscapedURL + "')");
#else
		Application.OpenURL(EscapedURL);
#endif
	}

	public static void ShareIntentCommon(string body)
	{
#if UNITY_IOS || UNITY_ANDROID
		OpenShareIntent(body);
#else
		OpenTwitterTweet(body);
#endif
	}

	public static void OpenTwitterTweet(string body)
	{
		OpenURL("https://twitter.com/intent/tweet?text=" + WWW.EscapeURL(body ) );
	}

	public static void OpenShareIntent(string body)
	{
		ShareBunch.GetInstance().ShareText(body);
	}

}
