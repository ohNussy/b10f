﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class BattlerData : MonoBehaviour
{

	[Header("Basic")]
	public double HP = 100;
	public double MaxHP = 100;

	public int Level = 1;
	public int Exp = 0;

	public Dictionary<string, double> ParamValues = new Dictionary<string, double>();

	public static BattlerData GetFromCharaData(GameObject Target, CharaData chara)
	{
		BattlerData NewBattler = Target.AddComponent<BattlerData>();

		// gain param by equip items
		foreach (StoredItemData item in chara.EquipList())
		{
			if (!object.ReferenceEquals(item, null))
			{
				foreach (StoredItemData.ItemParamSingleData sd in item.ParamList)
				{
					if (NewBattler.ParamValues.ContainsKey(sd.key))
					{
						MasterItemParam mip = (from m in Masters.GetAll<MasterItemParam>() where m.ParamKey == sd.key select m).FirstOrDefault();
						switch (mip.CalcType)
						{
							case MasterItemParam.CALC_TYPE_LVADD:
							case MasterItemParam.CALC_TYPE_SUMLV:
							case MasterItemParam.CALC_TYPE_VALLV:
							case MasterItemParam.CALC_TYPE_BOOL:
							case MasterItemParam.CALC_TYPE_SUM:
								NewBattler.ParamValues[sd.key] += sd.value;
								break;
							case MasterItemParam.CALC_TYPE_MAX:
								if (sd.value > NewBattler.ParamValues[sd.key])
									NewBattler.ParamValues[sd.key] = sd.value;
								break;
						}

					}
					else
					{
						NewBattler.ParamValues[sd.key] = sd.value;
                    }
				}
			}
		}

		return NewBattler;
	}

	public double GetParamValue(string key) { if (!ParamValues.ContainsKey(key)) return 0; return ParamValues[key]; }

	public double GetRealParam(string key)
	{
		MasterItemParam mip = (from m in Masters.GetAll<MasterItemParam>() where m.ParamKey == key select m).FirstOrDefault();
		switch (mip.CalcType)
		{
			case MasterItemParam.CALC_TYPE_LVADD:
				return GetParamValue(key) + mip.DefaultValue + Level;
			case MasterItemParam.CALC_TYPE_SUMLV:
			case MasterItemParam.CALC_TYPE_VALLV:
				return (GetParamValue(key) + mip.DefaultValue) * LevelRate;
			case MasterItemParam.CALC_TYPE_BOOL:
				return GetParamValue(key);
			case MasterItemParam.CALC_TYPE_SUM:
			case MasterItemParam.CALC_TYPE_MAX:
			default:
				return GetParamValue(key) + mip.DefaultValue;
		}
	}

	public double LevelRate { get { return Mathf.Pow(Level + 9, 2) / 100f; } }

}
