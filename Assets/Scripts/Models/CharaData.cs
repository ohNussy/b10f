﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class CharaData
{

	public static int CURRENCY_IDX_DUST = 0;
	public static int CURRENCY_IDX_SHARD = 1;
	public static int CURRENCY_IDX_STONE = 2;
	public static int CURRENCY_IDX_JEWEL = 3;
	public static int CURRENCY_IDX_KEY = 4;
	public static int CURRENCY_IDX_MEMORY = 5;

	public static int PARAM_CATEGORY_GENERAL = 0;
	public static int PARAM_CATEGORY_ATTACK = 1;
	public static int PARAM_CATEGORY_SPELL = 2;
	public static int PARAM_CATEGORY_MINION = 3;
	public static int PARAM_CATEGORY_DEFENCE = 4;

	public StoredCharaData Data;
	public Dictionary<int, double> TempVariedCurrency = new Dictionary<int, double>();

	public static CharaData Current { get { return UserData.Instance.CurrentChara; } }

	/*
	*	Init
	*/
	public void Init()
	{
		Data = new StoredCharaData();

		// first learn
		foreach (MasterPassive mp in Masters.GetAll<MasterPassive>())
			if (Mathf.Abs((float)mp.PosX) + Mathf.Abs((float)mp.PosY) == 1)
				Data.CanLearnPassives.Add(mp.ThisBase.ID);

		// debug
		{
			StoredItemData item = StoredItemData.NewItemSimple("IT000001");
			AddItem(item);
			SetEquip(1, item);
		}

		AfterLoad();
	}

	public void AfterLoad()
	{
		GainCurrency(CURRENCY_IDX_MEMORY, 20);
	}

	public static CharaData LoadedChara(StoredCharaData data)
	{
		CharaData chara = new CharaData();
		chara.Data = data;
		chara.AfterLoad();
		return chara;
	}

	/*
	*	References
	*/

	public string ImageUrl
	{
		get {
			if (!string.IsNullOrEmpty(Data.ImageURL))
				return Data.ImageURL;
			return "file://D:/Projects/unity/B10F-MemoriesNeverLast/Assets/ForBundles/Texture/Character/hero.png";
        }
	}
	
	public string ClearedLoopText
	{
		get
		{
			if (Data.ClearedLoop > 0)
				return string.Format("@{0}周目", Data.ClearedLoop.ToString());
			return "";
		}
	}

	/*
	*	Currency
	*/
	public double CurrentCurrency(int idx)
	{
		double gained = GainedCurrency(idx),
			used = UsedCurrency(idx);
		return gained - used;
	}

	public double GainedCurrency(int idx)
	{
		double gained = 0;
		if (Data.GainedCurrency.ContainsKey(idx))
			gained = Data.GainedCurrency[idx];
		return gained;
	}

	public double UsedCurrency(int idx)
	{
		double used = 0;
		if (Data.UsedCurrency.ContainsKey(idx))
			used = Data.UsedCurrency[idx];
		return used;
	}

	public string VaryingCurrency(int idx)
	{
		double value = CurrentCurrency(idx);
		if (TempVariedCurrency.ContainsKey(idx))
		{
			value += TempVariedCurrency[idx];
			if (TempVariedCurrency[idx] > 0)
			{
				return string.Format("<color=#0FF>{0}</color>", value.ToString("N0"));
			}
			else
			{
				return string.Format("<color=#F00>{0}</color>", value.ToString("N0"));
			}
		}
		return value.ToString("N0");
	}

	public bool EnoughCurrency(int idx, double value)
	{
		return (CurrentCurrency(idx) >= value);
	}

	public void PayCurrency(int idx, double value)
	{
		if (!Data.UsedCurrency.ContainsKey(idx))
			Data.UsedCurrency[idx] = 0;
		Data.UsedCurrency[idx] += value;
	}

	public void GainCurrency(int idx, double value)
	{
		if (!Data.GainedCurrency.ContainsKey(idx))
			Data.GainedCurrency[idx] = 0;
		Data.GainedCurrency[idx] += value;
	}

	public void GainItem(StoredItemData item)
	{
		if (Data.PossItems == null)
			Data.PossItems = new List<StoredItemData>();
        Data.PossItems.Add(item);
	}

	public void SetEquipItem(StoredItemData item, int idx)
	{
		Data.EquipItems[idx] = item.OpenKey;
	}

	public void RemoveEquipItem(int idx)
	{
		Data.EquipItems[idx] = string.Empty;
	}

	public StoredItemData GetEquipedItem(int idx)
	{
		if (!Data.EquipItems.ContainsKey(idx))
			return null;
		if (string.IsNullOrEmpty(Data.EquipItems[idx]))
			return null;
		return (from item in Data.PossItems where item.OpenKey == Data.EquipItems[idx] select item).FirstOrDefault();
	}

	public void SetVariedCurrency(int idx, double value)
	{
		TempVariedCurrency[idx] = value;
	}

	public void ApplyVariedCurrency()
	{
		foreach (int idx in TempVariedCurrency.Keys)
		{
			if (TempVariedCurrency[idx] > 0)
				GainCurrency(idx, TempVariedCurrency[idx]);
			else if (TempVariedCurrency[idx] < 0)
				PayCurrency(idx, -TempVariedCurrency[idx]);
		}
		ResetVariedCurrency();
	}

	public void ResetVariedCurrency()
	{
		TempVariedCurrency.Clear();
	}

	/*
	*	Passives
	*/

	public bool CanLearnPassive(MasterPassive mp)
	{
		if (HasPassive(mp))
			return false;
		return Data.CanLearnPassives.Contains(mp.ThisBase.ID);
	}

	public bool HasPassive(MasterPassive mp)
	{
		return Data.GainedPassives.Contains(mp.ThisBase.ID);
	}

	public void GainPassive(MasterPassive mp)
	{
		if (!CanLearnPassive(mp))
			return;
		if (HasPassive(mp))
			return;
		Data.GainedPassives.Add(mp.ThisBase.ID);
		// 習得可能
		Masters.GetAll<MasterPassive>().ForEach((nm) => {
			if (nm.IsNear(mp))
				if (!Data.CanLearnPassives.Contains(nm.ThisBase.ID))
					Data.CanLearnPassives.Add(nm.ThisBase.ID);
        });
	}

	/*
	*	Equips
	*/

	public List<StoredItemData> EquipList()
	{
		List<StoredItemData> list = new List<StoredItemData>();
		for (int idx = 0;idx < 5 ;idx++)
		{
			if (Data.EquipItems.ContainsKey(idx))
			{
				string id = Data.EquipItems[idx];
				StoredItemData item = (from i in Data.PossItems where i.OpenKey == id select i).FirstOrDefault();
                list.Add(item);
			}
			else
			{
				list.Add(null);
			}
		}
		return list;
    }

	public void SetEquip(int idx, StoredItemData item)
	{
		Data.EquipItems[idx] = item.OpenKey;
		// TODO : 装備時の調整、両手持ちなど
	}


	/*
	*	Items
	*/
	public StoredItemData AddItem(StoredItemData item)
	{
		Data.PossItems.Add(item);
		return item;
	}

}
