﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MasterBase : MonoBehaviour {

	MasterData _Master;

	public MasterData Master
	{
		get
		{
			if (object.ReferenceEquals(_Master, null))
				_Master = GetComponent<MasterData>();
			return _Master;
		}
	}

	public string ID
	{
		get
		{
			return Master.ID;
		}
	}

	public string DispName
	{
		get
		{
			return Master.DispName;
		}
	}

	public string DispDesc
	{
		get
		{
			return Master.DispDesc;
		}
	}

	public string DispImage
	{
		get
		{
			return Master.DispImage;
		}
	}

	public string GetString(string key)
	{
		return Master.GetString(key);
	}

	public double GetNumber(string key)
	{
		return Master.GetNumber(key);
	}

	public int Order
	{
		get
        {
			return (int)GetNumber("Order");
		}
	}

	public System.DateTime GetDate(string key)
	{
		return Master.GetDate(key);
	}

	public bool GetBool(string key)
	{
		return Master.GetBool(key);
	}


}
