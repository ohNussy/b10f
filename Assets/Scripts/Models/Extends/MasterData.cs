﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MasterData : MonoBehaviour {

	public Dictionary<string, string> Data;

	Dictionary<string, double> CachedNumbers = new Dictionary<string, double>();

	public string ID;
	public string DispName;
	public string DispDesc;

	public bool CheckData = false;

	public void SetData(Dictionary<string, string> Loaded)
	{
		Data = Loaded;
		ID = GetString("ID");
		DispName = GetString((GameData.IsJp ? "Name" : "NameEn"));
		DispDesc = GetString((GameData.IsJp ? "Desc" : "DescEn"));
		gameObject.name = string.Format("{0} {1} - {2}", gameObject.name.Replace("(Clone)", ""), ID, DispName);
	}
	
	public string DispImage
	{
		get
		{
			return GetString("Image");
		}
	}

	public string GetString(string key)
	{
		if (Data == null)
			return "";
		if (Data.ContainsKey(key))
			return Data[key];
		return "";
	}

	public double GetNumber(string key)
	{
		if (Data == null)
			return 0;
		if (CachedNumbers.ContainsKey(key))
			return CachedNumbers[key];
        if (Data.ContainsKey(key))
		{
			CachedNumbers[key] = double.Parse(Data[key]);
			return CachedNumbers[key];
		}
		return 0;
	}

	public System.DateTime GetDate(string key)
	{
		if (Data == null)
			return System.DateTime.MinValue;
		if (Data.ContainsKey(key))
			return System.DateTime.Parse(Data[key]);
		return System.DateTime.MinValue;
	}

	public bool GetBool(string key)
	{
		return (GetNumber(key) != 0);
	}

	void Update()
	{
#if UNITY_EDITOR
		if (CheckData)
		{
			string result = "";
			foreach (string key in Data.Keys)
				result += "[" + key + "] " + Data[key] + "\n";
			Debug.LogWarning(result);
			CheckData = false;
		}
#endif
	}

}
