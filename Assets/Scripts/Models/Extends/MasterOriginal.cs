﻿using UnityEngine;
using System.Collections;

public class MasterOriginal : MonoBehaviour
{

	MasterBase _mb;
	public MasterBase ThisBase
	{
		get
		{
			if (_mb == null)
				_mb = GetComponent<MasterBase>();
			return _mb;
		}
	}

}
