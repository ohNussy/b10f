﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LitJson;

public class StoredData
{

	public string OpenKey;

	private Dictionary<string, object> TempValues;

	public StoredData()
	{
		OpenKey = StringHelper.RandomString(8);
	}
	
	public string StoredJson()
	{
		return JsonMapper.ToJson(this);
	}

	public static Type LoadJson<Type>(string json) where Type : StoredData
	{
		return JsonMapper.ToObject<Type>(json);
	}

	public void SaveData(string key)
	{
		SaveHelper.SaveData(key, StoredJson());
	}

	public static Type LoadData<Type>(string key) where Type : StoredData
	{
		string json = SaveHelper.LoadData(key);
		if (string.IsNullOrEmpty(json))
			return null;
		return LoadJson<Type>(json);
    }

	public static void DeleteData(string key)
	{
		SaveHelper.Delete(key);
	}

	public string GetTempString(string key)
	{
		if (TempValues.ContainsKey(key))
			return TempValues[key].ToString();
		return string.Empty;
	}

	public double GetTempNumber(string key)
	{
		if (TempValues.ContainsKey(key))
			return double.Parse(TempValues[key].ToString());
		return 0;
	}

	public void SetTempValue(string key, object value)
	{
		if (TempValues == null)
			TempValues = new Dictionary<string, object>();
		TempValues[key] = value;
	}

}
