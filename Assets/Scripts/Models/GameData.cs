﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class GameData : Singleton<GameData> {

	public static GameData Instance
	{
		get
		{
			return ((GameData)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	[Header("Run Status")]
	public bool DebugMode = false;
	public bool LoggingMode = true;
	public bool AutoClearCache = false;
	public static bool IsDebugMode { get { return Instance.DebugMode; } }
	public string ClientVersion = "1.0.0";
	public static string ThisVersion { get { return Instance.ClientVersion; } }

	public static System.DateTime ServerTime;

	void Awake()
	{
		IsJp = (Application.systemLanguage == SystemLanguage.Japanese);
    }

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
		ClientVersion = UnityEditor.PlayerSettings.bundleVersion;
#endif   
	}

	// タッチデバイスか否か
	public static bool TouchDevice
#if UNITY_IOS || UNITY_ANDROID
		= true;
#else
		= false;
#endif

	// Web版か否か
	public static bool IsWeb
#if UNITY_WEBGL || UNITY_WEBPLAYER
		= true;
#else
		= false;
#endif

	public static bool IsJp = true;

	public static System.DateTime RealTime {
		get {
#if UNITY_EDITOR
			if (IsDebugMode)
				return System.DateTime.Now;
#endif
			return ServerTime.AddSeconds(Time.realtimeSinceStartup);
		}
	}

	public static string PlatformName()
	{
#if UNITY_WEBPLAYER
		return "Web";
#elif UNITY_WEBGL
		return "WebGL";
#elif UNITY_ANDROID
		return "Android";
#elif UNITY_IOS
		return "iOS";
#endif
		return "Other";
	}

	public static void Log(object val)
	{
		if (Instance.LoggingMode)
			Debug.Log(val);
	}

	public static void Warning(object val)
	{
		if (Instance.LoggingMode)
			Debug.LogWarning(val);
	}

	public static void Error(object val)
	{
		if (Instance.LoggingMode)
			Debug.LogError(val);
	}

}
