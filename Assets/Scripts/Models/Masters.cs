﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using LitJson;
using AvoEx;

public class Masters : Singleton<Masters>
{
	public static Masters Instance
	{
		get
		{
			return ((Masters)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	public Dictionary<string, List<MasterBase>> AllMasterList = new Dictionary<string, List<MasterBase>>();
	public Dictionary<string, MasterOriginal> MasterIDList = new Dictionary<string, MasterOriginal>();
	public bool MasterPrepared = false;

	public string[] MasterNames;

	public void Init(System.Action<bool> OnEnd)
	{
		AllMasterList.Clear();
		MasterIDList.Clear();
		LoadMasterByAB(MasterNames.First(), MasterNames.Skip(1).ToArray(), OnEnd);
	}

	void LoadMasterByAB(string key, string[] list, System.Action<bool> OnEnd)
	{
        AssetBundleManager.GetAsset<TextAsset>("master", key, (r, ta) => {
			// asset bundle check
			if (!r)
			{
				GameData.Error("LoadMasterByAb failed! @" + key);
				AsyncManager.RunBoolReturn(OnEnd, false);
				return;
			}
			// load json
			string json = AesEncryptor.DecryptString(ta.text);
			List<Dictionary<string, string>> datalist = JsonMapper.ToObject<List<Dictionary<string, string>>>(json);
			if (datalist == null)
			{
				GameData.Error("LoadMasterByAb failed on parse json! @" + key);
				AsyncManager.RunBoolReturn(OnEnd, false);
				return;
			}
			// load data
			GameObject RowPrefab = Resources.Load<GameObject>("Prefabs/Masters/Master" + key);
			AllMasterList[key] = new List<MasterBase>();
			// LoadMaster
            foreach (Dictionary<string, string> datarow in datalist)
			{
				GameObject go = GOHelper.AddNewUI(RowPrefab, transform);
				MasterData md = go.GetComponent<MasterData>();
				md.SetData(datarow);
				go.name = "Master" + key + ":" + md.ID + " - " + md.DispName;
				AllMasterList[key].Add(go.GetComponent<MasterBase>());
				MasterIDList[md.ID] = go.GetComponent<MasterOriginal>();
            }
			// after finish
			if (list.Count() > 0)
			{
				LoadMasterByAB(list.First(), list.Skip(1).ToArray(), OnEnd);
				return;
			}
			else
			{
				AsyncManager.RunBoolReturn(OnEnd, true);
			}
		});
	}

	public static T GetByID<T>(string ID) where T : MasterOriginal
	{
		if (Instance.MasterIDList.ContainsKey(ID))
		{
			return (T)Instance.MasterIDList[ID];
        }
		return null;
	}
	
	public static T GetFirst<T>(bool shuffle = false) where T : MasterOriginal
	{
		string MasterName = typeof(T).ToString().Replace("Master", "");
		return (from m in Instance.AllMasterList[MasterName] orderby (shuffle ? Random.Range(0, 100) : m.Order) select m).FirstOrDefault() as T;
	}

	public static List<T> GetAll<T>() where T : MasterOriginal
	{
		string MasterName = typeof(T).ToString().Replace("Master", "");
		MasterBase[] list = (from m in Instance.AllMasterList[MasterName] orderby m.Order select m).ToArray();
		List<T> result = new List<T>();
		foreach (MasterBase mb in list)
			result.Add(mb.GetComponent<MasterOriginal>() as T);
		list = null;
		return result;
	}

	public static List<T> GetByCond<T>(System.Func<T, bool> Condition, bool shuffle = false, bool UseOrder = false, System.Func<T, int> OrderFunc = null) where T : MasterOriginal
	{
		string MasterName = typeof(T).ToString().Replace("Master", "");
		MasterBase[] list = (from m in Instance.AllMasterList[MasterName]
							 where Condition(m as T)
							 orderby
							 (shuffle
								 ? Random.Range(0, 100) // シャッフル
							 : (UseOrder
								 ? OrderFunc(m.GetComponent<T>()) // 個別定義関数によるソート
								 : m.Order /* デフォルトオーダー */))
							 select m).ToArray();
		List<T> result = new List<T>();
		foreach (MasterBase mb in list)
			result.Add(mb.GetComponent<MasterOriginal>() as T);
		Debug.Log(result.Count);
		list = null;
		return result;
	}

	public static T GetOneByCond<T>(System.Func<T, bool> Condition, bool shuffle = false) where T : MasterOriginal
	{
		string MasterName = typeof(T).ToString().Replace("Master", "");
		return (from m in Instance.AllMasterList[MasterName]
				where Condition(m as T)
				orderby
				(shuffle ? Random.Range(0, 100) : m.Order)
				select m).FirstOrDefault().GetComponent<MasterOriginal>() as T;
	}

}
