﻿using UnityEngine;
using LitJson;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class MasterItem : MasterOriginal
{

	public double BaseItemLevel { get { return ThisBase.GetNumber("BaseItemLevel"); } }
	public int ItemType { get { return (int)ThisBase.GetNumber("ItemType"); } }
	public int Rarity { get { return (int)ThisBase.GetNumber("Rarity"); } }
	public int ActionType1 { get { return (int)ThisBase.GetNumber("ActionType1"); } }
	public int ActionType2 { get { return (int)ThisBase.GetNumber("ActionType2"); } }
	public bool TwoHand { get { return ThisBase.GetBool("TwoHand"); } }
	public bool MainHand { get { return ThisBase.GetBool("MainHand"); } }
	public bool SubHand { get { return ThisBase.GetBool("SubHand"); } }
	public string DataJson { get { return ThisBase.GetString("DataJson"); } }
	public string SpecialJson { get { return ThisBase.GetString("SpecialJson"); } }

	// 任意項目
	List<StoredItemData.ItemParamSingleData> _ParamList;
	public List<StoredItemData.ItemParamSingleData> ParamList
	{
		get
		{
			if (object.ReferenceEquals(_ParamList, null))
			{
				_ParamList = new List<StoredItemData.ItemParamSingleData>();
				foreach (string row in JsonMapper.ToObject<string[]>(DataJson))
				{
					string[] rows = row.Split(',');
					if (rows.Length != 2)
					{
						GameData.Warning("MasterItem can not load json row @" + ThisBase.ID + " :" + row);
						continue;
					}
					// extract
					string key = rows.First();
					double value = double.Parse(rows.Last());
					// set
					_ParamList.Add(new StoredItemData.ItemParamSingleData() { key = key, value = value });
				}
			}
			return _ParamList;
		}
	}

	// 装備固有
	List<StoredItemData.ItemParamSingleData> _SpecialList;
	public List<StoredItemData.ItemParamSingleData> SpecialList
	{
		get
		{
			if (object.ReferenceEquals(_SpecialList, null))
			{
				_SpecialList = new List<StoredItemData.ItemParamSingleData>();
				foreach (string row in JsonMapper.ToObject<string[]>(DataJson))
				{
					string[] rows = row.Split(',');
					if (rows.Length != 2)
					{
						GameData.Warning("MasterItem can not load json row @" + ThisBase.ID + " :" + row);
						continue;
					}
					// extract
					string key = rows.First();
					double value = double.Parse(rows.Last());
					// set
					_SpecialList.Add(new StoredItemData.ItemParamSingleData() { key = key, value = value });
				}
			}
			return _SpecialList;
		}
	}

}
