﻿using UnityEngine;
using System.Collections;

public class MasterItemParam : MasterOriginal
{

	public const int CALC_TYPE_SUM = 0;
	public const int CALC_TYPE_MAX = 1;
	public const int CALC_TYPE_SUMLV = 2;
	public const int CALC_TYPE_VALLV = 3;
	public const int CALC_TYPE_BOOL = 4;
	public const int CALC_TYPE_LVADD = 5;

	public int Category { get { return (int)ThisBase.GetNumber("Category"); } }
	public string ParamKey { get { return ThisBase.GetString("ParamKey"); } }
	public bool ViewDefault { get { return ThisBase.GetBool("ViewDefault"); } }
	public bool ViewValue { get { return ThisBase.GetBool("ViewValue"); } }
	public double DefaultValue { get { return ThisBase.GetNumber("DefaultValue"); } }
	public int CalcType	{ get { return (int)ThisBase.GetNumber("CalcType"); } }

}
