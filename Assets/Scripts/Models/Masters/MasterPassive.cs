﻿using UnityEngine;
using System.Collections;

public class MasterPassive : MasterOriginal
{

	public double PosX { get { return ThisBase.GetNumber("PosX"); } }
	public double PosY { get { return ThisBase.GetNumber("PosY"); } }
	public double MemoryCost { get { return ThisBase.GetNumber("MemoryCost"); } }

	public string CostColor {
		get {
			switch ((int)MemoryCost)
			{
				case 3:
					return "F00";
					break;
				case 2:
					return "FF0";
					break;
				case 1:
				default:
					return "0FF";
					break;
			}
		}
	}

	public bool IsNear(MasterPassive mp)
	{
		if (Mathf.Abs((float)PosX - (float)mp.PosX) + Mathf.Abs((float)PosY - (float)mp.PosY) <= 1)
			return true;
		return false;
	}

}
