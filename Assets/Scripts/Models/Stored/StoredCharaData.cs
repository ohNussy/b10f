﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StoredCharaData : StoredData
{

	// basic
	public string DispName = "No Name";
	public string ImageURL = "";

	// level
	public int CurrentLevel = 1;
	public double CurrentExp = 0;
	public double NeededExp = 0;

	// floor
	public int ClearedFloor = 0;
	public int ClearedLoop = 0;

	// currency
	public Dictionary<int, double> GainedCurrency = new Dictionary<int, double>();
	public Dictionary<int, double> UsedCurrency = new Dictionary<int, double>();

	// item
	public List<StoredItemData> PossItems = new List<StoredItemData>();
	public Dictionary<int, string> EquipItems = new Dictionary<int, string>();

	// passive
	public List<string> GainedPassives = new List<string>();
	public List<string> CanLearnPassives = new List<string>();

}
