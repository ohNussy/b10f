﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class StoredItemData : StoredData
{

	// basic
	public string DispName;
	public string DispImage;
	public string ItemMasterID = "";

	public double ItemLevel = 1;

	public double MixedCount = 0;

	public List<ItemParamSingleData> ParamList = new List<ItemParamSingleData>();

	// Cached Data
	public MasterItem ThisMaster() { return Masters.GetByID<MasterItem>(ItemMasterID); }

	public static StoredItemData NewItemSimple(string id)
	{
		MasterItem mi = Masters.GetByID<MasterItem>(id);
		StoredItemData item = new StoredItemData() { DispName = mi.ThisBase.DispName, DispImage = mi.ThisBase.DispImage, ItemLevel = mi.BaseItemLevel, ItemMasterID = id, OpenKey = StringHelper.RandomString(8) };
		foreach (ItemParamSingleData sd in mi.ParamList)
			item.ParamList.Add(new ItemParamSingleData() { key = sd.key, value = sd.value });
        return item;
	}

	// color
	public Color ItemBackColor
	{
		get {
			if (MixedCount > 0)
				return Color.yellow;
			switch (ThisMaster().Rarity)
			{
				case 4:
					return Color.magenta;
				case 3:
					return Color.green;
				case 2:
					return Color.cyan;
				case 1:
				default:
				return Color.white;
			}
		}
	}

	public class ItemParamSingleData
	{
		public string key;
		public double value;
	}

}
