﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StoredUserData : StoredData
{

	public static string SAVE_STORE_KEY = "UserBase";

	// 基本情報
	public string DispName = "No Name";

	public List<StoredCharaData> Charas = new List<StoredCharaData>();
	public string RecentChara = string.Empty;


}
