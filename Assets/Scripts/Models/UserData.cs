﻿using UnityEngine;
using LitJson;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class UserData : Singleton<UserData>
{

	public static UserData Instance
	{
		get
		{
			return ((UserData)mInstance);
		}
		set
		{
			mInstance = value;
		}
	}

	/*
	*	const
	*/
	public const int CHARA_NUM_MAX = 20;

	/*
	*	Data
	*/
	public StoredUserData BasicData;

	public CharaData CurrentChara;

	[Header("Data Viewer")]
	public string DispName;

	void Start()
	{
		StartCoroutine(Update30());
	}

	IEnumerator Update30()
	{
		while (true)
		{
			if (CanSave)
			{
				SaveImmediately();
			}
			yield return new WaitForSeconds(30);
		}
	}

	void Update()
	{
#if UNITY_EDITOR
#endif
	}

	bool CanSave = false;

	public void SaveData()
	{
		CanSave = true;
	}

	public void SaveImmediately()
	{
		CanSave = false;
		SaveHelper.SaveData(StoredUserData.SAVE_STORE_KEY, BasicData.StoredJson());
	}

	public StoredUserData LoadData()
	{
		string json = SaveHelper.LoadData(StoredUserData.SAVE_STORE_KEY);
		if (string.IsNullOrEmpty(json))
		{
			GameData.Warning("UserData LoadData Failed!");
			return null;
		}
		return JsonMapper.ToObject<StoredUserData>(json);
	}

	// return = User is loaded(false = new)
	public static bool DoLoadUserData()
	{
		StoredUserData data = UserData.Instance.LoadData();
		if (object.ReferenceEquals(data, null))
		{
			Instance.BasicData = new StoredUserData();
			Instance.AfterInit();
			return false;
		}
		else
		{
			Instance.BasicData = data;
			Instance.AfterLoad();
			return true;
		}
	}

	public void AfterInit()
	{

		AfterLoad();
	}

	public void AfterLoad()
	{

	}

	public void SetRecentChara()
	{
		if (string.IsNullOrEmpty(BasicData.RecentChara))
		{
			CurrentChara = CreateNewChara();
			return;
		}
		StoredCharaData data = (from c in BasicData.Charas where c.OpenKey == BasicData.RecentChara select c).FirstOrDefault();
		if (object.ReferenceEquals(data, null))
		{
			// blank
			CurrentChara = CreateNewChara();
			return;
		}
		CurrentChara = CharaData.LoadedChara(data);
	}

	StoredCharaData RecentChara()
	{
		return (from c in BasicData.Charas where c.OpenKey == BasicData.RecentChara select c).FirstOrDefault();
	}

	CharaData CreateNewChara()
	{
		CharaData NewChara = new CharaData();
		NewChara.Init();
		BasicData.Charas.Add(NewChara.Data);
		return NewChara;
    }


}
