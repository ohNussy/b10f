﻿using UnityEngine;
using System.Collections;

public class SelectableItemList : GridPanelBase
{
	public override void AfterSetData()
	{
		int idx = 0;
		foreach (SelectableItemRow row in GridList.GetComponentsInChildren<SelectableItemRow>())
		{
			row.PositionIdx = idx;
			idx++;
		}
	}
}
