﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class SelectableItemRow : GridRowBase
{

	public StoredItemData ThisItem;

	[Header("Targets")]
	public GameObject SelectingFrame;
	public GameObject DisableFrame;
	public Image ItemBack;
	public RawImage ItemIcon;
	public Text LblLevel;

	[Header("Data")]
	public int PositionIdx;
	public ICON_TYPE ActionType;

	public enum ICON_TYPE
	{
		EQUIP_LIST
		, SELECT_LIST
		, ITEM_LIST
		, MASS_SELL
		, TREASURE
		, BLANK
	}

	public override void SetData(RowDataBase Row)
	{
		base.SetData(Row);
		ThisItem = GetData<SelectableItemRowData>().item;
		if (object.ReferenceEquals(ThisItem, null))
		{
			SetBlank();
			return;
		}
		// visual
		ItemBack.gameObject.SetActive(true);
		ItemIcon.gameObject.SetActive(true);
		LblLevel.gameObject.SetActive(true);
		// visual detail
		ItemBack.color = ThisItem.ItemBackColor;
		TextureManager.SetImage(ItemIcon, ThisItem.DispImage, Color.white);
		LblLevel.text = string.Format("Lv{0}", ThisItem.ItemLevel.ToString());
	}

	public override void SetBlank()
	{
		SelectingFrame.SetActive(false);
		DisableFrame.SetActive(false);
		ItemBack.gameObject.SetActive(false);
		ItemIcon.gameObject.SetActive(false);
		LblLevel.gameObject.SetActive(false);
	}

	public void SetSelected(bool val)
	{
		SelectingFrame.SetActive(val);
	}

	public void SetEnable(bool val)
	{
		DisableFrame.SetActive(!val);
	}

	/*
	* Actions
	*/

	public override void OnClickEvent()
	{
		switch (ActionType)
		{
			case ICON_TYPE.EQUIP_LIST:
				ClickActionEquip();
                break;
			case ICON_TYPE.ITEM_LIST:
				break;
			case ICON_TYPE.MASS_SELL:
				break;
			case ICON_TYPE.SELECT_LIST:
				break;
			case ICON_TYPE.TREASURE:
				break;
		}
	}

	public override void OnClick2ndEvent()
	{
		switch (ActionType)
		{
			case ICON_TYPE.EQUIP_LIST:
				break;
			case ICON_TYPE.ITEM_LIST:
				ClickActionSelectList();
				break;
			case ICON_TYPE.MASS_SELL:
				break;
			case ICON_TYPE.SELECT_LIST:
				break;
			case ICON_TYPE.TREASURE:
				break;
		}
	}

	void ClickActionEquip()
	{
		List<StoredItemData> Items = (from it in CharaData.Current.Data.PossItems where it.ThisMaster().ItemType == 1 select it).ToList();
		DialogManager.OpenSelectItem(Items, (r, it) => {
			// on select
			if (r && !object.ReferenceEquals(it, null))
			{

			}
		});
	}

	void ClickActionSelectList()
	{

	}

	public override void OnMouseOnEvent()
	{
		
	}

	public override void OnMouseOutEvent()
	{
		
	}

	public class SelectableItemRowData : RowDataBase
	{
		public StoredItemData item;
	}
}
