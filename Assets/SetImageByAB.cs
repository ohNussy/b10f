﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetImageByAB : MonoBehaviour
{

	RawImage Target;
	public string key;
	public Color DefaultColor = Color.white;

	void OnEnable()
	{
		if (object.ReferenceEquals(Target, null))
			Target = GetComponent<RawImage>();
		if (object.ReferenceEquals(Target, null))
		{
			GameData.Error("SetImageByAB Failed! :" + name + " @" + key);
			return;
		}
		TextureManager.SetImage(Target, key, DefaultColor);
	}
}
