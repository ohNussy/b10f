﻿using UnityEngine;
using System.Collections;

public class SoundThrower : TouchableBehavior {

	public string key;

	public bool AutoCast = false;
	public bool CastWhenClick = false;
	public float CastTime = 0;
	public FIXED_SOUND ThisSound;

	public enum FIXED_SOUND
	{
		BLANK
		, OK
		, CANCEL
	}

	// Update is called once per frame
	public void DoAction ()
	{
		switch (ThisSound)
		{
			case FIXED_SOUND.OK:
				key = "ok";
				break;
			case FIXED_SOUND.CANCEL:
				key = "cancel";
				break;
		}
		AudioManager.PlaySE(key);
	}

	void OnEnable()
	{
		if (AutoCast)
		{
			AudioManager.Instance.StartCoroutine(CastSound());
		}
	}

	public override void OnClickEvent()
	{
		base.OnClickEvent();
		if (CastWhenClick)
			AudioManager.Instance.StartCoroutine(CastSound());
	}

	IEnumerator CastSound()
	{
		yield return new WaitForSeconds(CastTime);
		DoAction();
	}

}
